<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/*
*/
/*
*/
error_reporting(0);
class Profile extends CI_Controller
{

	public function __construct() {
		parent::__construct();
		$this->check_login();
	}

	public function check_login(){
		if(!$this->session->userdata('admin_id')){
			redirect('Admin/login');
		}
	}
 	
 	public function change_name_email(){
		
		$admin_id = $this->session->userdata('admin_id');

 		$this->form_validation->set_rules('name','name','trim|required');

 		if($this->form_validation->run()==true){
			 
			        $insert['name'] = $this->input->post('name');
					
			
				$run = $this->common_model->UpdateData('admin',array('id' =>$admin_id),$insert);

				if($run){

					
					$this->session->set_flashdata('msg','<div class="alert alert-success">Success! Your Profile Update successfully .</div>');

					redirect('admin/profile');
				} else {
					$this->session->set_flashdata('msg','<div class="alert alert-danger">We did not found any changes</div>');
					redirect('admin/profile');
				}

			
			
		} else {
			$this->session->set_flashdata('msg','<div class="alert alert-danger">'.validation_errors().'</div>');
			redirect('admin/profile');
		}

 	}
 	public function change_password(){

 		$admin_id = $this->session->userdata('admin_id');
		$users_data= $this->common_model->GetSingleData('admin',array('id'=>$admin_id));
 		

 		$this->form_validation->set_rules('admin_pass','Current password','required');
 		$this->form_validation->set_rules('New_Password','New password','required|min_length[6]');
 		$this->form_validation->set_rules('Confirm_Password','Confirm password','required|matches[New_Password]');

 		if($this->form_validation->run()==true){

			$admin_pass = $this->input->post('admin_pass');
	
			$password = password_hash($this->input->post('New_Password'), PASSWORD_DEFAULT);

            if (password_verify($admin_pass,$users_data['password'])) {

 				$run = $this->common_model->UpdateData('admin',array('id' =>$admin_id),array('password'=>$password));

 				if($run){

					$this->session->set_flashdata('msg','<div class="alert alert-success">Success! Your password update successfully.</div>');
					redirect('admin/profile');

				}else {
					$this->session->set_flashdata('msg','<div class="alert alert-danger">Error! Something went to wrong.</div>');
					redirect('admin/profile');
				}
 			}
 			else{

 				$this->session->set_flashdata('msg','<div class="alert alert-danger">Error! Current password is not matched.</div>');
				redirect('admin/profile');
 			}
 		}
 		else{
 			$this->session->set_flashdata('msg','<div class="alert alert-danger">'.validation_errors().'</div>');
			redirect('admin/profile');
 		}
 	}
 	public function index(){
		$this->load->view('Admin/profile');
	} 



}


 ?>