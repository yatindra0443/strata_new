<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Home extends CI_Controller
{
	
	public function __construct() {
		parent::__construct();
		$this->check_login();
	}

	public function check_login(){
		if(!$this->session->userdata('admin_id')){
			redirect('index');
		}
	}

	public function index(){
		$user_id = $this->session->userdata('admin_id');
		$type = $this->session->userdata('admin_type');
		$this->load->view('Admin/index');
	}

	public function contact(){
		$user_id = $this->session->userdata('admin_id');
		$type = $this->session->userdata('admin_type');
		$data['contact'] = $this->common_model->GetAllData('contact_us','','id');
		$this->load->view('Admin/contact',$data);
	}
	
	public function logout(){
		session_destroy();
		redirect('admin/login');
	}

	

}

?>