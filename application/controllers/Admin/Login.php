<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->check_login();
	}
	public function check_login(){
		if($this->session->userdata('admin_id')){
			redirect('Admin/dashboard');
		}
	}
	public function index(){
		$this->load->view('Admin/login');
	}
	public function do_login(){

		$this->form_validation->set_rules('email','Email','required');
		$this->form_validation->set_rules('password','Password','required');

		if($this->form_validation->run()){
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			
			$run = $this->common_model->GetSingleData('admin',array('email' =>$email));
			//echo $this->db->last_query();
			if($run) {
				if (password_verify($password,$run['password'])) {
				    $this->session->set_userdata('admin_id',$run['id']);
					$this->session->set_flashdata('msg','<div class="alert alert-success">Welcome '.$run['name'].'</div>');

					redirect('admin/dashboard');
				} else {
				    $this->session->set_flashdata('msg','<div class="alert alert-danger">Password incorrect.</div>');
				    redirect('admin/login');
				}
							
			} else {
				
				$this->session->set_flashdata('msg','<div class="alert alert-danger">username or password incorrect.</div>');
				redirect('admin/login');
			}
		 } else {
			$this->session->set_flashdata('msg','<div class="alert alert-danger">'.validation_errors().'</div>');
			redirect('admin/login');
		}
	}

}
?>