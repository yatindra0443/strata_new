<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class User extends CI_Controller
{
	
	public function __construct() {
		parent::__construct();
		$this->check_login();
		$this->load->model('Common_function');
	}

	public function check_login(){
		if(!$this->session->userdata('admin_id')){ 
			redirect('index');
		}
	}

	public function index(){
		$data['pageName'] = "New user";
		$user_id = $this->session->userdata('admin_id');
		
		$data['user'] = $this->common_model->GetAllData('user',array('profile_status'=>0, 'step_completed' => 1),'id');
		$this->load->view('Admin/user-list',$data);
	}

	public function approve_user(){
		$data['pageName'] = "Approve user";
		$user_id = $this->session->userdata('admin_id');
		$data['user'] = $this->db->query("select * from user where profile_status = 2 AND step_completed < 4 order by id desc")->result_array(); 
		$this->load->view('Admin/user-list',$data);
	}

	public function waiting_user(){
		$data['pageName'] = "Waiting user";
		$user_id = $this->session->userdata('admin_id');
		$data['user'] = $this->common_model->GetAllData('user',array('profile_status'=>1),'id');
		$this->load->view('Admin/user-list',$data);
	}

	// public function reject_user(){
	// 	$data['pageName'] = "Reject user";
	// 	$user_id = $this->session->userdata('admin_id');
	// 	$data['user'] = $this->common_model->GetAllData('user',array('profile_status'=>2),'id');
	// 	$this->load->view('Admin/user-list',$data);
	// }

	public function active_user(){
		$data['pageName'] = "Active user";
		$user_id = $this->session->userdata('admin_id');
		
		$data['user'] = $this->common_model->GetAllData('user',array('status'=>1,'profile_status'=>2, 'step_completed' => 4),'id');
		$this->load->view('Admin/user-list',$data);
	}

	public function inactive_user(){
		$data['pageName'] = "Inactive user";
		$user_id = $this->session->userdata('admin_id');
		$data['user'] = $this->common_model->GetAllData('user',array('status'=>0),'id');
		$this->load->view('Admin/user-list',$data);
	}

    public function change_Profile_status(){
		
		$admin_id = $this->session->userdata('admin_id');
	
		$user_id= $this->input->post('userId');
		$status= $this->input->post('status');
			 
			//  if($status==1){
			// 	$run = $this->common_model->UpdateData('user',array('id' =>$user_id),array('profile_status'=>$status,'step_completed'=>3));
            //   } else {
            //   	$run = $this->common_model->UpdateData('user',array('id' =>$user_id),array('profile_status'=>$status));
            //   }
		$run = $this->common_model->UpdateData('user',array('id' =>$user_id),array('profile_status'=>$status));

		if($run){
			$user = $this->common_model->GetColumnName('user',array('id' =>$user_id),array('fcm_token','uuid'));
			if($status==1){
				$this->Common_function->fcmPushNotification('Profile Waitlisted','Your application status has been changed.',$user['uuid'],$user['fcm_token']);  
				$this->session->set_flashdata('msg','<div class="alert alert-success">Success! User Profile has Waitlisted successfully .</div>');
				echo "1";
			} else {
				$this->Common_function->fcmPushNotification('Profile Approved','Congratulations! Your application to Strata has been accepted.',$user['uuid'],$user['fcm_token']);  
				$this->session->set_flashdata('msg','<div class="alert alert-success">Success! User profile has approved successfully .</div>');
				echo "2";
			}
		} else {
			$this->session->set_flashdata('msg','<div class="alert alert-danger">We did not found any changes</div>');
			echo "0";
		}

 	}

 	  public function change_status(){
		
			$admin_id = $this->session->userdata('admin_id');
			$user_id= $this->input->post('userId');
			$status= $this->input->post('status');
				$run = $this->common_model->UpdateData('user',array('id' =>$user_id),array('status'=>$status));

				if($run){
                   if($status=='1'){
                     	$this->session->set_flashdata('msg','<div class="alert alert-success">Success! This profile has been activated successfully .</div>');
                    	echo "1";
                   } else {
                    	$this->session->set_flashdata('msg','<div class="alert alert-success">This profile has been deactivated successfully.</div>');
                    	echo "2";
                   }
				} else {
					$this->session->set_flashdata('msg','<div class="alert alert-danger">We did not found any changes</div>');
					echo "0";
				}

 	}

 	public function add_waiting_list(){
		
		$admin_id = $this->session->userdata('admin_id');
	
		$user_id= $this->input->post('userId');
		$status= $this->input->post('status');
		$run = $this->common_model->UpdateData('user',array('id' =>$user_id),array('profile_status'=>1));

		if($run){
			$user = $this->common_model->GetColumnName('user',array('id' =>$user_id),array('fcm_token','uuid'));
			$this->Common_function->fcmPushNotification('Profile Waitlisted','Your application status has been changed.',$user['uuid'],$user['fcm_token']);  
			$this->session->set_flashdata('msg','<div class="alert alert-success">Success! This profile has been waitlisted successfully .</div>');
			echo "1";
		} else {
			$this->session->set_flashdata('msg','<div class="alert alert-danger">We did not found any changes</div>');
			echo "0";
		}

 	}
	

}

?>