<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Version extends CI_Controller
{
	
	public function __construct() {
		parent::__construct();
		$this->check_login();
		$this->load->model('Common_function');
	}

	public function check_login(){
		if(!$this->session->userdata('admin_id')){ 
			redirect('index');
		}
	}

	public function index(){
		$data['pageName'] = "Version";
		$user_id = $this->session->userdata('admin_id');
		$data['versionData'] = $this->common_model->GetAllData('app_version',array(),'device_type, currentVersion, forceUpdate');
		$this->load->view('Admin/version',$data);
	}

	public function update_app_version(){
		$data['pageName'] = "Update Version";
		$user_id = $this->session->userdata('admin_id');
		$id = $this->input->post('id');
		$currentVersion = $this->input->post('currentVersion');
		$forceUpdate = $this->input->post('forceUpdate');
		$updateData = $this->common_model->UpdateData('app_version',array('id' =>$id),array("currentVersion"=>$currentVersion, 'forceUpdate'=>$forceUpdate));
		//echo $this->db->last_query(); exit;
		if($updateData){
			echo "true";
		}else {
			echo "false";
		}
		exit;
	}
}
?>