<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
// require APPPATH . 'libraries/REST_Controller.php';
//require APPPATH . 'libraries/JWT.php';
class Auth extends REST_Controller {
    function __construct() {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        $this->load->library('uuid');
        $this->load->model('Common_function');
    }
    /**
     * Function - login_post
     * Description - User login
     * callingRoute - login
     * Created on - 30 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function login_post() {
        $json = $this->Common_function->get_json_parameter(array()); //check required parameter
        if ($json) {
            $countryCode = $json->countryCode;
            $phone = $json->phone; //Username Posted
            if(strlen($phone)!=10 || $countryCode!= '91'){
                errorResponse(required_data, REST_Controller::HTTP_BAD_REQUEST);
            }
            $dataArray = array('country_code' => $countryCode, 'phone' => $phone, 'is_deleted' => 0); //For where query condition
            if($phone != '9669636960') {
                $otp = rand(1000,9999);
            }else {
                $otp = '4304';
            }
            
            $check = $this->common_model->GetColumnName('user', $dataArray, array('uuid', 'profile_status as profileStatus', 'status', 'step_completed as stepCompleted'));
            if ($check == true) {
                if ($check['status'] == 0 && $check['stepCompleted'] == 4) {
                    errorResponse(change_status, REST_Controller::HTTP_BAD_REQUEST);
                }
                if($check['status'] == 1 && $check['stepCompleted'] == 4) {
                    $run = $this->common_model->UpdateData('user', $dataArray, array('otp' => $otp));  
                }
                $check['isSuspended'] = false;
                if($phone != '9669636960'){
                    $this->Common_function->send_msg($otp, $countryCode.''.$phone);
                }
                successResponse(send_otp, REST_Controller::HTTP_OK, $check);
            } else {
                errorResponse(phone_notregistred, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }
    // public function login_post() {
    //     $json = $this->Common_function->get_json_parameter(array()); //check required parameter
    //     if ($json) {
    //         $countryCode = $json->countryCode;
    //         $phone = $json->phone; //Username Posted
    //         $dataArray = array('country_code' => $countryCode, 'phone' => $phone, 'is_deleted' => 0); //For where query condition
    //         // $otp = rand(1000,9999);
    //         $otp = '1234';
    //         $check = $this->common_model->GetColumnName('user', $dataArray, array('uuid', 'profile_status', 'status'));
           
    //         if ($check == true) {
    //             if ($check['profile_status'] == 0) {
    //                 errorResponse(profile_status_pending, REST_Controller::HTTP_BAD_REQUEST);
    //             } else if ($check['status'] == 0) {
    //                 errorResponse(change_status, REST_Controller::HTTP_BAD_REQUEST);
    //             } else if ($check['profile_status'] > 1) {
    //                 //$this->Common_function->msg91('Your otp is ' . $otp, $countryCode . '' . $phone);
    //                 $run = $this->common_model->UpdateData('user', $dataArray, array('otp' => $otp));
    //                 if ($run) {
    //                     successResponse(send_otp, REST_Controller::HTTP_OK, ['uuid' => $check['uuid'],'isSuspended'=>false]);
    //                 } else {
    //                     errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUT);
    //                 }
    //             } else {
    //                 errorResponse(profile_status_waitlisted, REST_Controller::HTTP_BAD_REQUEST);
    //             }
    //         } else {
    //             errorResponse(phone_notregistred, REST_Controller::HTTP_BAD_REQUEST);
    //         }
    //     }
    // }
    /**
     * Function - send_otp_post
     * Description - send_otp
     * callingRoute - send_otp
     * Created on - 30 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function send_otp_post() {
        $json = $this->Common_function->get_json_parameter(array()); //check required parameter
        if ($json) {
            $countryCode = $json->countryCode;
            $phoneNumber = $json->phone; //Username Posted
            if(strlen($phoneNumber)!=10 || $countryCode!= '91'){
                errorResponse(required_data, REST_Controller::HTTP_BAD_REQUEST);
            }
            $phonecheck = $this->common_model->GetColumnName('user', array('country_code' => $countryCode, 'phone' => $phoneNumber), array('id', 'verify_otp', 'step_completed','uuid'));
            $this->load->helper('string');
            if($phone != '9669636960') {
                $otp = rand(1000,9999);
            }else {
                $otp = '4304';
            }
            if ($phonecheck == true) {
                $data = array('country_code' => $countryCode, 'phone' => $phoneNumber, 'otp' => $otp);
                $uuid = $phonecheck['uuid'];
                if ($phonecheck['verify_otp'] == 0) {
                    $run = $this->common_model->UpdateData('user', array('id' => $phonecheck['id']), $data);
                } else if ($phonecheck['step_completed'] == 0) {
                    $run = $this->common_model->UpdateData('user', array('id' => $phonecheck['id']), $data);
                } else {
                    errorResponse(phone_registred, REST_Controller::HTTP_BAD_REQUEST);
                    die;
                }
            } else {
                $uuid = $this->uuid->v4();
                $data = array('country_code' => $countryCode, 'phone' => $phoneNumber, 'uuid' => $uuid, 'otp' => $otp);
                $run = $this->common_model->InsertData('user', $data);
            }
            if($phone != '9669636960') {
                $this->Common_function->send_msg($otp, $countryCode.''.$phoneNumber);
            }
            if ($run) {
                successResponse(send_otp, REST_Controller::HTTP_OK, ['uuid' => $uuid]);
            } else {
                errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
        }else {
            errorResponse(required_data, REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    /**
     * Function - verify_otp_post
     * Description - verify otp using otp and uuid
     * callingRoute - verify_otp
     * Created on - 30 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function verify_otp_post() {
        $json = $this->Common_function->get_json_parameter(array("fcmToken" => "")); //check required parameter
        if ($json) {
            $otp = $json->otp;
            $countryCode = $json->countryCode;
            $phoneNumber = $json->phone; //Pasword Posted
            $kunci = $this->config->item('secret_key');
            if(strlen($phoneNumber)!=10 || $countryCode!= '91'){
                errorResponse(required_data, REST_Controller::HTTP_BAD_REQUEST);
            }
            $val = $this->common_model->GetSingleData('user', array('country_code' => $countryCode, 'phone' => $phoneNumber)); //Model to get single data row from database base on username
            if ($val) {
                if ($val['otp'] == $otp) {
                    $run = $this->common_model->UpdateData('user', array('country_code' => $countryCode, 'phone' => $phoneNumber), array('otp' => '', 'verify_otp' => 1, 'fcm_token' => $json->fcmToken));
                    if ($val['email']) {
                        $user['uuid'] = $val['uuid'];
                        $user['name'] = $val['name'];
                        $user['stepCompleted'] = $val['step_completed'];
                        $user['email'] = $val['email'];
                        $token['id'] = $val['id']; //From here
                        $token['uuid'] = $val['uuid'];
                        $token['name'] = $val['name'];
                        $token['gender'] = $val['gender'];
                        $token['age'] = $val['age'];
                        $token['age_range_from'] = $val['age_range_from'];
                        $token['age_range_to'] = $val['age_range_to'];
                        $interested_in = array();
                        $intrested = $this->common_model->GetAllData('interested', array('user_id' => $val['id']));
                        foreach ($intrested as $key => $value) {
                            $interested_in[$key] = $value['interested_in'];
                        }
                        $token['interested_in'] = $interested_in;
                        $date = new DateTime();
                        $token['iat'] = $date->getTimestamp();
                        $token['exp'] = $date->getTimestamp() + 60 * 60 * 5; //To here is to generate token
                        $output = JWT::encode($token, $kunci); //This is the output token
                        $run = $this->common_model->UpdateData('user', array('id' => $val['id']), array('token' => $output));
                        unset($val['token']);
                        header("Authorization: Bearer $output");
                        successResponse(login_success, REST_Controller::HTTP_OK, $user);
                    } else {
                        successResponse(verify_otp, REST_Controller::HTTP_OK, ['uuid' => $val['uuid']]);
                    }
                } else {
                    errorResponse(otp_incorrect, REST_Controller::HTTP_BAD_REQUEST);
                }
            } else {
                errorResponse("User is not registred", REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }
    /**
     * Function - registration_step1_post
     * Description - signup with required parameter
     * callingRoute - signup
     * Created on - 30 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function registration_step1_post() {
        $json = $this->Common_function->get_json_parameter(array("linkdinProfile" => "","aboutUs"=>"")); //check required parameter
        if ($json) {
            $val = $this->common_model->GetSingleData('user', array('country_code' => $json->countryCode, 'phone' => $json->phone));
            if ($val) {
                if($val['verify_otp']!=1) {
                    errorResponse("Otp not verified!", REST_Controller::HTTP_REQUEST_TIMEOUT);
                }
                if($val['step_completed']==1){
                   errorResponse('Already registred', REST_Controller::HTTP_REQUEST_TIMEOUT);
                } else {
                    $email = $insert['email'] = $json->email;
                    $phone = $insert['phone'] = $json->phone;
                    $country_code = $insert['country_code'] = $json->countryCode;
                    $insert['name'] = $json->name;
                    $insert['dob'] = $json->dob;
                    $insert['about_us'] = $json->aboutUs;
                    $insert['country_id'] = $json->countryId;
                    $insert['country_name'] = $json->countryName;
                    $insert['instagram'] = $json->instagram;
                    $insert['age_range_from'] = 18;
                    $insert['age_range_to'] = 70;
                    $insert['created_at'] = date('Y-m-d H:i:s');
                    $insert['updated_at'] = date('Y-m-d H:i:s');
                    $insert['step_completed'] = 1;
                    $insert['status'] = 1;
                    $this->load->helper('string');
                
                    $insert['age'] = $this->Common_function->ageCalculator($json->dob);
                
                    $membership_code = $insert['membership_code'] = mt_rand(10000, 99999);
                    //Output a v4 UUID
                    $run = $this->common_model->UpdateData('user', array('phone' => $phone), $insert);
                    if ($run) {
                        successResponse(success_registration, REST_Controller::HTTP_OK, ['uuid' => $val['uuid'],'membershipCode' => $membership_code]);
                    } else {
                        errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUT);
                    }
                }
            } else {
                errorResponse(phone_notregistred, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }
    public function checkStatusCode_post() {
        $json = $this->Common_function->get_json_parameter(array()); //check required parameter
        if ($json) {
            $data = $this->common_model->GetColumnName('user', array('uuid' => $json->uuid), array('uuid', 'profile_status as profileStatus', 'step_completed as stepCompleted'));
            $data['isSuspended'] =false;
            if ($data) {
                successResponse('Data get successfully', REST_Controller::HTTP_OK, $data);
            } else {
                errorResponse(user_not_registred, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }
    /**
     * Function - registration_step2_post
     * Description - complete profile with required parameter
     * callingRoute - complete_profile
     * Developer -
     * Created on - 30 may 2022
     * Modify on - 05 july 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function complete_profile_post() {
        if ($this->post('uuid') && $this->post('step') ) {
            $check = $this->common_model->GetColumnName('user', array('uuid' => $this->post('uuid')), array('uuid', 'id'));
            if ($check == true) {
                $steps = $this->post('step');
                if($steps == 2) {
                    if(!empty($_FILES['images']['name']) && count(array_filter($_FILES['images']['name'])) > 0){
                        $filesCount = count($_FILES['images']['name']);
                        $upload = $this->Common_function->upload_images('assets/images/user_img/', 'images');
                            // $run2 = $this->common_model->UpdateData('user', array('uuid' => $check['uuid']), array('profile_pic' => base_url() . 'assets/images/user_img/' . $upload['name'][0],));
                            if ($filesCount > 3 && $filesCount <= 8) {  
                                $i = 0;
                                foreach ($upload['name'] as $key => $value) {
                                    $uuid = $this->uuid->v4();
                                    $insert_data_arr[] = "('','".$uuid."',".$check['id'].",'".$value."', ".$i." ,'".date("Y-m-d H:i:s")."')";
                                    $i++;
                                }
                                if($insert_data_arr) {
                                    $implode = implode(',',$insert_data_arr);
                                    $this->db->query("INSERT INTO images VALUES ".$implode."");
                                }
                            } else {
                                errorResponse(image_validation, REST_Controller::HTTP_BAD_REQUEST);
                            }
                    }  
                    $insert['about_us'] = $this->input->post('aboutUs');
                    $insert['here_for'] = $this->input->post('hereFor');
                }else if($steps == 3){
                    $interested_in = $this->input->post('interestedIn');
                    $interested_in = json_decode($interested_in, TRUE);
                    if (count($interested_in) > 0) {
                        foreach ($interested_in as $key => $value) {
                            $insert1['uuid'] = $this->uuid->v4();
                            $insert1['interested_in'] = $value;
                            $insert1['user_id'] = $check['id'];
                            $run1 = $this->common_model->InsertData('interested', $insert1);
                        }
                        
                    } else {
                        errorResponse(required_data, REST_Controller::HTTP_BAD_REQUEST);
                    }
                    $insert['gender'] = $this->input->post('gender');
                    $insert['current_city'] = $this->input->post('currentCity');
                    $insert['orignal_city'] = $this->input->post('orignalCity');
                }else if($steps == 4) {
                    $insert['title'] = $this->input->post('title');
                    $insert['organization'] = $this->input->post('organization');
                    $insert['linkdin_profile'] = $this->input->post('linkdinProfile');
                    if($this->input->post('workLink')){
                        $work_link = json_decode($this->input->post('workLink'));
                        foreach ($work_link as $key => $value) {
                            $insert2['uuid'] = $this->uuid->v4();
                            $insert2['work_link'] = $value;
                            $insert2['user_id'] = $check['id'];
                            $run2 = $this->common_model->InsertData('work_link', $insert2);
                        }
                    } if($this->input->post('occupation')){
                        $insert['occupation'] = $this->input->post('occupation');
                    }
                    $insert['status'] = '1';
                }else {
                    errorResponse(required_data, REST_Controller::HTTP_BAD_REQUEST);
                }
                $insert['step_completed'] = $steps;
                $insert['updated_at'] = date('Y-m-d H:i:s');
                $run = $this->common_model->UpdateData('user', array('uuid' => $check['uuid']), $insert);
                $val = $this->common_model->GetSingleData('user', array('uuid' => $check['uuid'])); //Model to get single data row from database base on username
                        $user['uuid'] = $val['uuid'];
                        $user['name'] = $val['name'];
                        $user['stepCompleted'] = $val['step_completed'];
                        $user['email'] = $val['email'];
                        $user['gender'] = $val['gender'];
                        $user['age'] = $val['age'];
                        $user['age_range_from'] = $val['age_range_from'];
                        $user['currentCity'] = $val['current_city'];
                        $user['orignalCity'] = $val['orignal_city'];
                        $user['organization'] = $val['organization'];
                        $user['linkdinProfile'] = $val['linkdin_profile'];
                        $user['occupation'] = $val['occupation'];
                        $interested_in = array();
                        $intrested = $this->common_model->GetAllData('interested', array('user_id' => $val['id']));
                        foreach ($intrested as $key => $value) {
                            $interested_in[$key] = $value['interested_in'];
                        }
                        $user['interestedIn'] = $interested_in;
                        $images = array();
                        $image = $this->common_model->GetAllData('images', array('user_id' => $val['id']));
                        if($image) {
                            $output['profile'] = base_url() . 'assets/images/user_img/'.$image[0];
                            foreach ($image as $key => $row) {
                                $images[$key]['uuid'] = $row['uuid'];
                                $images[$key]['images'] = base_url() . 'assets/images/user_img/'.$row['image'];
                            }
                        }
                        $user['images'] = $images;
                        $workLink = array();
                        $work = $this->common_model->GetAllData('work_link', array('user_id' => $val['id']));
                        if($work) {
                            foreach ($work as $key => $value) {
                                $workLink[$key]['uuid'] = $value['uuid'];
                                $workLink[$key]['workLink'] = $value['work_link'];
                            }
                        }
                        $user['workLink'] = $workLink;
                        if($steps==4){
                            $token['id'] = $val['id']; //From here
                            $token['uuid'] = $val['uuid'];
                            $token['name'] = $val['name'];
                            $token['gender'] = $val['gender'];
                            $token['age'] = $val['age'];
                            $token['age_range_from'] = $val['age_range_from'];
                            $token['age_range_to'] = $val['age_range_to'];
                            $token['interested_in'] = $interested_in;
                            $date = new DateTime();
                            $token['iat'] = $date->getTimestamp();
                            $token['exp'] = $date->getTimestamp() + 60 * 60 * 5; //To here is to generate token
                            $output = JWT::encode($token, $kunci); //This is the output token
                            $run = $this->common_model->UpdateData('user', array('id' => $val['id']), array('token' => $output));
                            header("Authorization: Bearer $output");
                        }
                        successResponse(updated_data, REST_Controller::HTTP_OK, $user);
                
            } else {
                errorResponse(user_not_registred, REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            errorResponse(required_data, REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    /**
     * Function - upload_images_post
     * Description - upload user images with uuid
     * callingRoute - upload_images
     * Developer -
     * Created on - 30 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function upload_images_post() {
        if ($this->post('uuid') && !empty($_FILES['images']['name']) && count(array_filter($_FILES['images']['name'])) > 0) {
            $check = $this->common_model->GetColumnName('user', array('uuid' => $this->post('uuid')), array('uuid', 'id'));
            if ($check == true) {
                //Output a v4 UUID
                $uuid = $insert['uuid'] = $this->uuid->v4();
                $filesCount = count($_FILES['images']['name']);
                if ($filesCount > 3 && $filesCount <= 8) {
                    //echo $fileData['file_name'];
                    $upload = $this->Common_function->upload_images('assets/images/user_img/', 'images');
                    foreach ($upload['name'] as $key => $value) {
                        $uploadData['user_id'] = $check['id'];
                        $uploadData['uuid'] = $uuid;
                        $uploadData['image'] = $value;
                        $run = $this->common_model->InsertData('images', $uploadData);
                    }
                    successResponse(add_image, REST_Controller::HTTP_OK, ['userUuid' => $check['uuid']]);
                } else {
                    errorResponse(image_validation, REST_Controller::HTTP_BAD_REQUEST);
                }
            } else {
                errorResponse(user_not_registred, REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            errorResponse(required_data, REST_Controller::HTTP_BAD_REQUEST);
        }
    }
    /**
     * Function - contact_list_post
     * Description - get Contact List data is registred true or false and return data
     * callingRoute - signup
     * Developer -
     * Created on - 30 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function contact_list_post() {
        $json = $this->Common_function->get_json_parameter(array());
        if ($json) {
            $contactList = $json->contact_list;
            $user_uuid = $json->uuid;
            $uuid = $this->uuid->v4();
            $data = array();
            $user = $this->common_model->GetColumnName('user', array('uuid' => $user_uuid), array('id'));
            foreach ($contactList as $key => $value) {
                //$data[$key] = $value->phone;
                $data[] = "('', ".$user['id'].",'".$value->phone."','".date("Y-m-d H:i:s")."')";
            }
            if($data) {
                $implodeData = implode(',',$data);
                $this->db->query('INSERT INTO temp_contact_list VALUES '.$implodeData.'');
            }
            //$string_version ='"'.implode('","', $data).'"';
            
            // $this->db->query("SET SESSION group_concat_max_len = 1000000");
            //$check = $this->db->query("select phone,name,profile_pic as profile,user.uuid from user INNER JOIN temp_contact_list  where phone IN ($string_version) and status=1 and is_deleted=0 and uuid != '" . $user_uuid . "' and id NOT IN( select to_userId from invite where from_userId=user.id) ")->result_array();
            $checkContact = $this->db->query("SELECT DISTINCT(phone),name,profile_pic as profile,user.uuid FROM `user` INNER JOIN temp_contact_list ON temp_contact_list.contact_no in (CONCAT(country_code,'',phone), phone, REPLACE(CONCAT(country_code,'',phone), '+', ''), CONCAT('0','',phone) ) WHERE user.phone NOT IN (SELECT phone from common_connection WHERE user_id = ".$user['id']." AND phone = user.phone) AND status=1 and user.is_deleted=0 and user.uuid != '" . $user_uuid . "' AND user.step_completed = 4")->result_array();
            //echo $this->db->last_query(); exit;
            //print_r($checkContact);
            $deleteTempTableData = $this->db->query("DELETE FROM temp_contact_list WHERE user_id = ".$user['id']."");

            if ($checkContact) {
                foreach ($check as $key => $value) {
                    $insert_data_arr[] = "('','".$uuid."',".$user['id'].",'".$value['phone']."','".date("Y-m-d H:i:s")."')";
                }
                if($insert_data_arr) {
                    $implode = implode(',',$insert_data_arr);
                    $this->db->query("INSERT INTO common_connection VALUES ".$implode."");
                }
                successResponse('Data get successfully', REST_Controller::HTTP_OK, $checkContact);
            }
            errorResponse(contact_error, REST_Controller::HTTP_BAD_REQUEST);
            
        }
    }
    // public function contact_list_post() {
    //     $json = $this->Common_function->get_json_parameter(array());
    //     if ($json) {
    //         $contactList = $json->contact_list;
    //         $user_uuid = $json->uuid;
    //         $uuid = $this->uuid->v4();
    //         $data = array();
    //         foreach ($contactList as $key => $value) {
    //             $data[$key] = $value->phone;
    //         }
    //         $string_version ='"'.implode('","', $data).'"';
            
    //         $user = $this->common_model->GetColumnName('user', array('uuid' => $user_uuid), array('id'));
    //         $this->db->query("SET SESSION group_concat_max_len = 1000000");
    //         $check = $this->db->query("select phone,name,profile_pic as profile,user.uuid from user where phone IN ($string_version) and status=1 and is_deleted=0 and uuid != '" . $user_uuid . "' and id NOT IN( select to_userId from invite where from_userId=user.id) ")->result_array();
    //        //echo $this->db->last_query();
    //         foreach ($check as $key => $value) {
    //             $insert_data_arr[] = "('','".$uuid."',".$user['id'].",'".$value['phone']."','".date("Y-m-d H:i:s")."')";
    //         }
    //         if($insert_data_arr) {
    //             $implode = implode(',',$insert_data_arr);
    //             $this->db->query("INSERT INTO common_connection VALUES ".$implode."");
    //         }
    //         if ($check) {
    //             successResponse('Data get successfully', REST_Controller::HTTP_OK, $check);
    //         } else {
    //             errorResponse(contact_error, REST_Controller::HTTP_BAD_REQUEST);
    //         }
    //     }
    // }

    public function test_post() {
        
        //  $json = $this->Common_function->get_json_parameter();
        //  if($json->type==1){
        //    $run=  $this->Common_function->msg91('Your otp is 123456',$json->value);
        //  } else {
        //   $run=  $this->Common_function->Fcmpushnotification('Your account has been approved','1',$json->value);  
        //  }
        $images = $this->Common_function->upload_images('user_profile'); 
         print_r($images);
        // if ($images) {
        //     successResponse("Success", REST_Controller::HTTP_OK, $images);
        // } else {
        //     errorResponse("Error image", REST_Controller::HTTP_REQUEST_TIMEOUT, $images);
        // }
    }

    /**
     * Function - invite_post
     * Description - invite_post
     * callingRoute - invite
     * Created on - 17 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function invite_post() {
        $json = $this->Common_function->get_json_parameter(); //check required parameter
        if ($json) {
                $insert['uuid'] = $this->uuid->v4();
                $from_user = $this->common_model->GetColumnName('user', array('uuid' => $json->fromUserUuid),array('id'));
                $insert['from_userId'] = $from_user['id'];
                $insert['created_at'] = date('Y-m-d H:i:s');
            $toUser = $this->db->query("select id, fcm_token from user where uuid IN('".implode("','", $json->toUserUuid)."')")->result_array();
            foreach ($toUser as $key => $value) {
                    
                $insert['to_userId'] = $value['id'];
                $run = $this->common_model->InsertData('invite', $insert); 
                $tokenArr[] = $value['fcm_token'];
            }
            $this->Common_function->fcmPushNotification('STRATA','You have new reference request.','',$tokenArr);  

            if ($run) {
                successResponse("Invite sent succesfully", REST_Controller::HTTP_OK);
            } else {
                errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
        }
    }
    /**
     * Function - commonConnection
     * Description - Get user common connection
     * callingRoute - common_connection
     * Created on - 5 Aug 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function appVersion_get() {
        $app = $this->input->get('deviceType');
        $check = $this->db->query('select currentVersion, forceUpdate from app_version Where device_type = "'.$app.'"')->row();
        successResponse("Success", REST_Controller::HTTP_OK, $check);
    }

    //test notification 
    public function testNotification_post() {
        $json = $this->Common_function->get_json_parameter(array("title"=>"")); //check required parameter
        $this->Common_function->fcmPushNotification($json->title,$json->message,'',$json->token); 
        successResponse("Success", REST_Controller::HTTP_OK, '');
    }
}
