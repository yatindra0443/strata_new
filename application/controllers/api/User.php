<?php
 error_reporting(0); 
class User extends BD_Controller {
    /**
     * Get All Data from this method.
     *
     * @return Response
     */
  
    public function __construct() {
        parent::__construct();
        $this->auth();
        $this->load->model('Common_function');
        $this->load->library('uuid');
    }
    /**
     * Get All Data from this method.
     *
     * @return Response
     */
    public function profile_get() {
        $data = array();
        if (!empty($this->user_data)) {
            $user = $this->user_data;
            $output = $this->common_model->GetColumnName('user', array('id' => $user->id, 'is_deleted' => 0),array('uuid','name','email','country_code as countryCode','phone','dob','gender','age','current_city as currentCity','orignal_city as orignalCity','country_id as countryId','country_name as countryName','linkdin_profile as linkdinProfile','about_us as aboutUs','membership_code as membershipCode','instagram','occupation','title','organization','here_for as hereFor','age_range_from as ageRangeFrom','age_range_to as ageRangeTo','profile_pic as profile','pause_profile as pauseProfile','push_notification as pushNotification','status','profile_status as profileStatus','fcm_token as fcmToken','created_at as createdAt','updated_at as updatedAt','is_deleted as isDeleted'));
            $images = array();
            $image = $this->common_model->GetAllData('images', array('user_id' => $user->id), 'image_index', 'ASC');
            $output['profile'] = base_url() . 'assets/images/user_img/' .$image[0]['image'];

            foreach ($image as $key => $row) {
                $images[$key]['uuid'] = $row['uuid'];
                $images[$key]['images'] = base_url() . 'assets/images/user_img/' .$row['image'];
            }
            $work_link = array();
            $work = $this->common_model->GetAllData('work_link', array('user_id' => $user->id));
            foreach ($work as $key => $value) {
                $work_link[$key]['uuid'] = $value['uuid'];
                $work_link[$key]['workLink'] = $value['work_link'];
            }
             $intresteIn = array();
            $interested = $this->common_model->GetAllData('interested', array('user_id' => $user->id));
            foreach ($interested as $key => $value) {
                $intresteIn[$key]['uuid'] = $value['uuid'];
                $intresteIn[$key]['interestedIn'] = $value['interested_in'];
            }
            $output['images'] = $images;
            $output['workLink'] = $work_link;
            $output['intresteIn'] = $intresteIn;
           
            if ($output) {
                successResponse(Get_user, REST_Controller::HTTP_OK, $output);
            } else {
                errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
        }
    }
    /**
     * Function - update_profile_post
     * Description - update profile using token id
     * callingRoute - update_prodile
     * Created on - 06 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function update_profile_post() {
        //print_r($params , 'worklink');exit;
        if (!empty($this->user_data)) {
            $user = $this->user_data;
            //header('Content-Type : application/json;charset=UTF-8');
            $params = $this->Common_function->get_required_parameter(array('workLink'=>"","linkdinProfile"=>"","aboutUs"=>"","deletedImage"=>"","title" =>"")); //check required paramter
                $insert['current_city'] = $params['currentCity'];
                $insert['orignal_city'] = $params['orignalCity'];
                $insert['title'] = $params['title'];
                $insert['organization'] = $params['organization'];
                $insert['linkdin_profile'] = $params['linkdinProfile'];
                $insert['name'] = $params['name'];
                if($params['aboutUs']){
                $insert['about_us'] = $params['aboutUs'];
                }
                if(!empty($params['workLink'])) {
                    $workLink = json_decode($this->Common_function->remove_utf8_bom($params['workLink']));
                }else {
                    $workLink = array();
                }
                if(!empty($params['deletedImage'])) {
                    $deletedImage = explode(',',$params['deletedImage']);
                }else {
                    $deletedImage = array();
                }
                //print_r($deletedImage); exit;
                // Note: calulate count of images need to work in future if required
                //$imagedata = $this->db->query('select count(id) as imageCount from images where user_id = '.$user->id.' ')->row()->imageCount;
                $filesCount = (isset($_FILES['images']['name'])) ? count($_FILES['images']['name']) : 0;
                // $totalCount = ($imagedata)+$filesCount-count($deletedImage);
                // if (($totalCount < 4 || $totalCount > 8) ) {
                //     errorResponse(image_validation, REST_Controller::HTTP_BAD_REQUEST);
                // }
                
                 // image upload code
                if($filesCount > 0){
                    $lastIndex = $this->db->query('select image_index from images where user_id = '.$user->id.' order by image_index desc limit 1')->row()->image_index;  
                    $upload = $this->Common_function->upload_images('assets/images/user_img/', 'images');
                    foreach ($upload['name'] as $key => $value) {
                        $lastIndex = ($lastIndex>0) ? $lastIndex++ : 0;
                        $uuid = $this->uuid->v4();
                        $insert_data_arr[] = "('','".$uuid."',".$user->id.",'".$value."', $lastIndex ,'".date("Y-m-d H:i:s")."')";
                    }
                    if($insert_data_arr) {
                        $implode = implode(',',$insert_data_arr);
                        $this->db->query("INSERT INTO images VALUES ".$implode."");
                    }
                    $insert['updated_at'] = date('Y-m-d H:i:s');
                }
                // for deletetig images
                if(count($deletedImage) > 0){
                    // $deletedImages = $params['deletedImage'];
                    // $deletedImages= json_decode($deletedImages);
                    foreach ($deletedImage as $imageDelete) {
                        //write code for delete image from S3
                        $img = $this->common_model->GetSingleData('images',array('uuid'=>$imageDelete));
                        $path = base_url().'assets/images/user_img/'.$img['image'];
                        unlink($path);
                    }
                    $deleteImg = $this->db->query("delete from images where uuid IN('".implode("','", $deletedImage)."')");
                }

                for ($i=0; $i < count($workLink) ; $i++) { 
                   //echo $workLink[$i]['workLink'];exit;
                    if ($workLink[$i]->uuid) {
                        $insert1['work_link'] = $workLink[$i]->workLink;
                        $run1 = $this->common_model->UpdateData('work_link', array('uuid' => $workLink[$i]->uuid), $insert1);
                    } else {
                        $uuid = $this->uuid->v4();
                        $run1 = $this->common_model->InsertData('work_link', array('work_link' => $workLink[$i]->workLink, 'user_id' => $user->id,'uuid'=>$uuid));
                    }
                }
                // foreach ($workLink as $key => $value) {
                //         echo $value; 
                //         if ($value->uuid) {
                //             $insert1['work_link'] = $value->workLink;
                //             $run1 = $this->common_model->UpdateData('work_link', array('uuid' => $value->uuid), $insert1);
                //         } else {
                //             $uuid = $this->uuid->v4();
                //             $run1 = $this->common_model->InsertData('work_link', array('work_link' => $value->workLink, 'user_id' => $user->id,'uuid'=>$uuid));
                //         }
                //     }
                //     exit;
                $run = $this->common_model->UpdateData('user', array('id' => $user->id), $insert);
                successResponse(preference_updated, REST_Controller::HTTP_OK);
              
        }
        
    }
    // public function update_profile_post() {
    //     if (!empty($this->user_data)) {
    //             $user = $this->user_data;
           
    //             $insert['current_city'] = $this->input->post('currentCity');
    //             $insert['orignal_city'] = $this->input->post('orignalCity');
    //             $insert['title'] = $this->input->post('title');
    //             $insert['organization'] = $this->input->post('organization');
    //             $insert['linkdin_profile'] = $this->input->post('linkdinProfile');
    //             if($this->input->post('aboutUs')){
    //             $insert['about_us'] = $this->input->post('aboutUs');
    //             }
    //             $work_link = $this->input->post('workLink');
    //             //Output a v4 UUID
    //             // for deletetig images
    //             $deletedImages = $this->input->post('deletedImage');
    //             if($deletedImages) {
    //                 $deletedImages= json_decode($deletedImages);
    //                 foreach ($deletedImages as $delete) {
    //                     $img = $this->common_model->GetSingleData('images',array('uuid'=>$delete));
    //                     $path = base_url().'assets/images/user_img/'.$img['image'];
    //                     unlink($path);
    //                     $deleteImg = $this->common_model->DeleteData('images',array('uuid'=>$delete));
    //                 }
    //             }
    //  * Function - increaseScreenshotCount
    //  * Description - update_fcmToken_post
    //  * callingRoute - update_fcmToken
    //  * Created on - 27 jun 2022
    //  * @param {*} req Request object contains post type

    //         }

    //         if ($run) {
    //             successResponse(update_profile, REST_Controller::HTTP_OK);
    //         } else {
    //             errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUT);
    //         }
    //       }


    /**
     * Function - update_profile_post
     * Description - update profile using token id
     * callingRoute - update_prodile
     * Created on - 06 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function update_prefrence_post() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
           $json = $this->Common_function->get_json_parameter(array());
           if($json){
         
                $insert['age_range_from'] = $json->ageRangeFrom;
                $insert['age_range_to'] = $json->ageRangeTo;
                $interested_in = $json->interestedIn;
                $insert['updated_at'] = date('Y-m-d H:i:s');
                $run = $this->common_model->UpdateData('user', array('id' => $user->id), $insert);
                $deleteData = $this->common_model->DeleteData('interested',array('user_id'=>$user->id));
                foreach ($interested_in as $key => $value) {
                    $insert1['uuid'] = $this->uuid->v4();
                    $insert1['interested_in'] = $value;
                    $insert1['user_id'] = $user->id;
                    $rundata = $this->common_model->InsertData('interested', $insert1);
                }
                if ($run) {
                    successResponse(preference_updated, REST_Controller::HTTP_OK);
                } else {
                    errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUT);
                }   
            }
        }
    }
    /**
     * Function - logout
     * Description - logout using token
     * callingRoute - logout
     * Created on - 09 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function logout_get() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
            $run = $this->common_model->UpdateData('user', array('id' => $user->id), array('token' => ''));
            if ($run) {
                successResponse('logout user', REST_Controller::HTTP_OK);
            } else {
                errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
        }
    }
    /**
     * Function - delete_account_get
     * Description - delete account
     * callingRoute - delete_account
     * Created on - 09 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function delete_account_get() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
            //$run = $this->common_model->UpdateData('user', array('id' => $user->id), array('token' => '', 'is_deleted' => 1, 'verify_otp'=>0, 'status' => 0, 'step_completed' => 0));
            $run = $this->common_model->DeleteData('user', array('id' => $user->id));
            if ($run) {
                //$this->common_model->DeleteData('images', array('user_id' => $user->id));
                //$this->common_model->DeleteData('interested', array('user_id' => $user->id));
                successResponse(account_delete, REST_Controller::HTTP_OK);
            } else {
                errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
        }
    }
    /**
     * Function - like_dislike_post
     * Description - Like or dislike users
     * callingRoute - like_dislike
     * Created on - 09 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
   public function like_dislike_post() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
            $json = $this->Common_function->get_json_parameter(array("status" => "0")); //check required parameter
            if ($json) {
                $insert['uuid'] = $this->uuid->v4();
                $from_user = $this->common_model->GetColumnName('user', array('uuid' => $json->fromUserUuid),array('id'));
                $to_user = $this->common_model->GetColumnName('user', array('uuid' => $json->toUserUuid),array('id', 'fcm_token'));
                $insert['from_userId'] = $from_user['id'];
                $insert['to_userId'] = $to_user['id'];
                $status = $insert['status'] = $json->status;
                $insert['created_at'] = date('Y-m-d H:i:s');
                $like = $this->common_model->GetColumnName('like_tbl', array('from_userId' => $from_user['id'],
                'to_userId' => $to_user['id']),array('status','id'));
                if ($like) {
                    // if($like['status']== $insert['status']){
                    //     //return error msg action already performed
                    // }
                    $run = $this->common_model->UpdateData('like_tbl', array('id' => $like['id']), array('status'=>$status));
                } else {
                    $run = $this->common_model->InsertData('like_tbl', $insert);
                }
            
               $output = false;
                if ($status == 1) {
                    $msg = "User added in like";
                    $matchData = $this->common_model->GetSingleData('like_tbl', array('from_userId' => $to_user['id'], 'to_userId' => $from_user['id'],'status'=>1));
                    if($matchData){
				        $this->Common_function->fcmPushNotification('STRATA','You have a new connection!','',$to_user['fcm_token']);  
                        $output = true;
                        //Note:- update match status in table
                    }else {
    				    $this->Common_function->fcmPushNotification('STRATA','You have one like, see who is your match','',$to_user['fcm_token']);  
                    }  
                } else {

                    $msg = "User removed in like";
                }
                if ($run) {
                    successResponse($msg, REST_Controller::HTTP_OK,["isMatched"=>$output]);
                } else {
                    errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUT);
                }
            }
        }
    }
    /**
     * Function - like_list_get
     * Description - invite_list_get
     * callingRoute - invite_list
     * Created on - 17 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function like_list_get() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
            $inviteData = $this->common_model->getLikeData(array('a.from_userId' => $user->id, 'a.status' => 1));
            if ($inviteData) {
                successResponse("Get like User list successfully", REST_Controller::HTTP_OK, $inviteData);
            } else {
                errorResponse("No data found", REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
        }
    }
    /**
     * Function - profile_setting_post
     * Description - profile_setting_post
     * callingRoute - profile_setting
     * Created on - 17 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function profile_setting_post() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
            $json = $this->Common_function->get_json_parameter(array("pauseProfile" => 0, "pushNotification" => 0, "hereFor" => 0)); //check required parameter
            if ($json) {
                $update['pause_profile'] = $json->pauseProfile;
                $update['push_notification'] = $json->pushNotification;
                $update['here_for'] = $json->hereFor;
                $run = $this->common_model->UpdateData('user', array('id' => $user->id), $update);
                if ($run) {
                    successResponse("profile setting updated succesfully", REST_Controller::HTTP_OK);
                } else {
                    errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUTHTTP_REQUEST_TIMEOUTHTTP_REQUEST_TIMEOUT);
                }
            }
        }
    }
    
    /**
     * Function - accept_reject_invite_post
     * Description - accept_reject_invite_post
     * callingRoute - accept_reject_invite
     * Created on - 17 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function accept_reject_invite_post() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
            $json = $this->Common_function->get_json_parameter(); //check required parameter
            if ($json) {
                $status = $insert['status'] = $json->status;
                $id = $json->inviteUuid;
                $run = $this->common_model->UpdateData('invite', array('uuid' => $id), $insert);
                if ($run) {
                    if ($status == 1) {
                        $invite_msg = "invite accept succesfully";
                    } else {
                        $invite_msg = "invite reject succesfully";
                    }
                    successResponse($invite_msg, REST_Controller::HTTP_OK);
                } else {
                    errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUT);
                }
            }else {
                errorResponse(required_data, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }
    /**
     * Function - invite_list_get
     * Description - invite_list_get
     * callingRoute - invite_list
     * Created on - 19 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function invite_list_get() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
            $inviteData = $this->common_model->getinviteData(array('a.to_userId' => $user->id, 'a.status' => 0));
            if ($inviteData) {
                successResponse("Get invite user list successfully", REST_Controller::HTTP_OK, $inviteData);
            } else {
                errorResponse("No data found", REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
        }
    }
    /**
     * Function - block_unblock_post
     * Description - block_unblock users
     * callingRoute - block_unblock
     * Created on - 19 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function block_unblock_post() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
            $json = $this->Common_function->get_json_parameter(array("status" => "0")); //check required parameter
            if ($json) {

                $insert['uuid'] = $this->uuid->v4();
                $from_user = $this->common_model->GetColumnName('user', array('uuid' => $json->fromUserUuid),array('id'));
                $to_user = $this->common_model->GetColumnName('user', array('uuid' => $json->toUserUuid),array('id'));
                $insert['from_userId'] = $from_user['id'];
                $insert['to_userId'] = $to_user['id'];
                $status = $insert['status'] = $json->status;
                $insert['reason'] = $json->reason;
                $insert['created_at'] = date('Y-m-d H:i:s');
                $block = $this->common_model->GetSingleData('block_user', array('from_userId' => $from_user['id'], 'to_userId' => $to_user['id']));
                if ($block) {
                    $run = $this->common_model->UpdateData('block_user', array('id' => $block['id']), $insert);
                    if ($status == 1) {
                        $msg = "User Blocked";
                    } else {
                        $msg = "User Unblock";
                    }
                } else {
                    $run = $this->common_model->InsertData('block_user', $insert);
                    $msg = "User blocked";
                }
                if ($run) {
                    successResponse($msg, REST_Controller::HTTP_OK);
                } else {
                    errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUT);
                }
            }else {
                errorResponse(required_data, REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }
    /**
     * Function - block_list_get
     * Description - block_list_get
     * callingRoute - block_list
     * Created on - 19 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function block_list_get() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
            $blockData = $this->common_model->getblockData(array('a.from_userId' => $user->id, 'a.status' => 1));
            if ($blockData) {
                successResponse("Get Block User list successfully", REST_Controller::HTTP_OK, $blockData);
            } else {
                errorResponse("No data found", REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
        }
    }
    /**
     * Function - matching_list_get
     * Description - block_list_get
     * callingRoute - block_list
     * Created on - 19 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function matching_list_get() {

        if (!empty($this->user_data)) {
            $user = $this->user_data;
            $interested_in = array();
            $page = $this->input->get('page'); //check required parameter
            // if(!$page){
            //     errorResponse(required_data, REST_Controller::HTTP_BAD_REQUEST);
            // }
            $page = ($page) ? $page : 1;
            $limit = 10;
            $offset = ($page == 1) ? 0 : $limit*($page-1);

            $intrested = $this->common_model->GetAllData('interested', array('user_id' => $user->id));
            foreach ($intrested as $key => $value) {
                $interested_in[$key] = $value['interested_in'];
            }
            if($interested_in){
                $intrestedIn = implode(',', $interested_in);
            } else {
                $intrestedIn = (0);    
            }

            $likeData = $this->db->query("select COALESCE(GROUP_CONCAT(to_userId separator ','),0) as to_userId from like_tbl where from_userId = " . $user->id . "")->result_array();
            //echo $this->db->last_query();

            $blockData = $this->db->query("select COALESCE(GROUP_CONCAT(to_userId separator ','),0) as to_userId from block_user where from_userId = " . $user->id . " and status=1")->result_array();

            $workLinks = $this->db->query("select COALESCE(GROUP_CONCAT(id separator ','),0) as worklinkId from work_link
            ")->result_array();
            $check = $this->db->query("select a.uuid,phone,name, here_for as hereFor, gender,age,current_city as currentCity,orignal_city as orignalCity,country_name as countryName,linkdin_profile as linkdinProfile,about_us as aboutUs,instagram,occupation,title,organization, COALESCE(GROUP_CONCAT(interested.interested_in separator ','),0) as interested_in, (select COALESCE(GROUP_CONCAT( work_link separator ','),'') from work_link where user_id = a.id) as workLink, (select COALESCE(GROUP_CONCAT(image separator ','),'') from images where user_id = a.id order by image_index asc) as images, age_range_from, age_range_to  from user as a INNER JOIN interested ON a.id = interested.user_id LEFT JOIN work_link ON a.id = work_link.user_id INNER JOIN (SELECT user.uuid 
             FROM  user WHERE  age_range_from <= ".$user->age." and age_range_to >= ".$user->age." and status=1 and profile_status=2 and is_deleted=0
            ) b ON a.uuid = b.uuid WHERE a.age between " . $user->age_range_from . " and " . $user->age_range_to . " and a.status=1 and a.profile_status= 2  and a.gender IN (" . $intrestedIn . ") and a.is_deleted=0 AND a.id NOT IN(".$likeData[0]['to_userId'].") and a.id NOT IN(".$blockData[0]['to_userId'].") and a.id!=" . $user->id . "  group by a.id order by a.id DESC LIMIT ".$offset.",".$limit."")->result_array();
            //echo $this->db->last_query(); exit;
            $resArr = array();
            $images = array();
            if(count($check) > 0){
                foreach ($check as $key => $users) {
                    if($user->age >= $users['age_range_from'] && $user->age <= $users['age_range_to'] && (strpos($users['interested_in'], $user->gender) !== false)) {
                        if($users['workLink']){
                            $workLink =  explode(',',$users['workLink']);
                            $workLinkArr = array();
                            for ($j=0; $j < count($workLink); $j++) { 
                                $workLinkArr[] = array("workLink" =>$workLink[$j]);
                            }
                            $check[$key]['workLink'] = $workLinkArr;
                        }else {
                            $check[$key]['workLink'] = [];
                        }
                        if($users['images']){
                            $images = explode(',',$users['images']);
                            $imagesArr = array();
                            for ($i=0; $i < count($images); $i++) { 
                                $imagesArr[] = array("images"=>PROFILE_IMAGE.$images[$i]);
                            }
                            $check[$key]['images'] = $imagesArr ;
                        }else {
                            $check[$key]['images'] = []; 
                        }
                        unset($check[$key]['interested_in']);
                        $resArr[] = $check[$key];
                    }
                }
            }
            if ($resArr) {
                successResponse(" Matching List ", REST_Controller::HTTP_OK, $resArr);
            } else {
                errorResponse(" No data found ", REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
        }
    }
   
    /**
     * Function - block_list_get
     * Description - block_list_get
     * callingRoute - block_list
     * Created on - 19 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function disabled_list_get() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
            $blockData = $this->common_model->getblockData(array('a.from_userId' => $user->id, 'a.status' => 1));
            if ($blockData) {
                successResponse("Get Block User list successfully", REST_Controller::HTTP_OK, $blockData);
            } else {
                errorResponse("No data found", REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
        }
    }
    /**
     * Function - like_dislike_post
     * Description - Like or dislike users
     * callingRoute - like_dislike
     * Created on - 09 may 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function undo_post() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
            $json = $this->Common_function->get_json_parameter(array()); //check required parameter
            if ($json) {
              $users = $this->common_model->GetColumnName('user',array('uuid'=>$json->userUuid),array('id'));
                if($users) {
                    $like = $this->common_model->GetSingleData('like_tbl',array('from_userId'=>$user->id,'to_userId'=>$users['id'],'status'=>0));
                    if($like) {
                        $run = $this->common_model->DeleteData('like_tbl', array('id' => $like['id']));
                        if ($run) {
                            successResponse("Successfully Undo", REST_Controller::HTTP_OK); 
                        } 
                    }
                }
            }
            errorResponse("Record not found ", REST_Controller::HTTP_REQUEST_TIMEOUT); 
        }
    }

    public function unmatch_post() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
             $json = $this->Common_function->get_json_parameter(array("status" => "0")); //check required parameter
            if ($json) {
              $users = $this->common_model->GetColumnName('user',array('uuid'=>$json->userUuid),array('id'));
            $like = $this->common_model->GetSingleData('like_tbl',array('from_userId'=>$user->id,'to_userId'=>$users['id'],'status'=>1));
            $run = $this->common_model->DeleteData('like_tbl', array('id' => $like['id']));
            if ($run) {
                successResponse("Delete data", REST_Controller::HTTP_OK);
            } else {
                errorResponse("Record not found ", REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
          }
        }
    }

     /**
     * Function - update_fcmToken_post
     * Description - update_fcmToken_post
     * callingRoute - update_fcmToken
     * Created on - 27 jun 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function update_fcmToken_post() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
             $json = $this->Common_function->get_json_parameter(); //check required parameter
            if ($json) {
         $updateDtaa = $this->common_model->UpdateData('user',array('id'=>$user->id),array('fcm_token'=>$json->fcmToken));

            if ($updateDtaa) {
                successResponse("Fcm Token updated successfully", REST_Controller::HTTP_OK);
            } else {
                errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
          }
        }
    }

     /**
     * Function - getUserListById_get
     * Description - getUserListById_get
     * callingRoute - get_user_list_by_id
     * Created on - 27 jun 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function getUserListById_post() {
      
            $data = array();
        if (!empty($this->user_data)) {
            $user = $this->user_data;
              $json = $this->Common_function->get_json_parameter(); //check required parameter
            if ($json) {
            $output = $this->common_model->GetColumnName('user', array('uuid' => $json->userUuid, 'is_deleted' => 0),array('id','uuid','name','email','country_code as countryCode','phone','dob','gender','age','current_city as currentCity','orignal_city as orignalCity','country_id as countryId','country_name as countryName','linkdin_profile as linkdinProfile','about_us as aboutUs','membership_code as membershipCode','instagram','occupation','title','organization','here_for as hereFor','age_range_from as ageRangeFrom','age_range_to as ageRangeTo','profile_pic as profile','pause_profile as pauseProfile','push_notification as pushNotification','status','profile_status as profileStatus','fcm_token as fcmToken','created_at as createdAt','updated_at as updatedAt','is_deleted as isDeleted'));
            $images = array();
         
            $image = $this->common_model->GetAllData('images', array('user_id' =>$output['id']),'image_index', 'ASC');
            foreach ($image as $key => $row) {
                $images[$key]['uuid'] = $row['uuid'];
                $images[$key]['images'] = base_url() . 'assets/images/user_img/' .$row['image'];
            }
            $work_link = array();
            $work = $this->common_model->GetAllData('work_link', array('user_id' => $output['id']));
            foreach ($work as $key => $value) {
                $work_link[$key]['uuid'] = $value['uuid'];
                $work_link[$key]['work_link'] = $value['work_link'];
            }
             $intresteIn = array();
            $interested = $this->common_model->GetAllData('interested', array('user_id' => $output['id']));
            foreach ($interested as $key => $value) {
                $intresteIn[$key]['uuid'] = $value['uuid'];
                $intresteIn[$key]['interestedIn'] = $value['interested_in'];
            }
            $output['images'] = $images;
            $output['workLink'] = $work_link;
            $output['intresteIn'] = $intresteIn;
             unset($output['id']);
            if ($output) {
                successResponse(Get_user, REST_Controller::HTTP_OK, $output);
            } else {
                errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
        }
      }
       
    }
    
    /**
     * Function - increaseScreenshotCount
     * Description - update_fcmToken_post
     * callingRoute - update_fcmToken
     * Created on - 27 jun 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function increaseScreenShotCount_post() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
          $get = $this->common_model->GetColumnName('user',array('id'=>$user->id),array('screenShotCount')); 
          $totalCount = $get['screenShotCount']+1;  
         $updateDtaa = $this->common_model->UpdateData('user',array('id'=>$user->id),array('screenShotCount'=>$totalCount));

            if ($updateDtaa) {
                successResponse("ScreenShot Count updated successfully", REST_Controller::HTTP_OK);
            } else {
                errorResponse(query_error, REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
          
        }
    }

     /**
     * Function - commonConnection
     * Description - Get user common connection
     * callingRoute - common_connection
     * Created on - 5 Aug 2022
     * @param {*} req Request object contains post type
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function commonConnection_get() {

        if (!empty($this->user_data)) {
            $user = $this->user_data;
            $interested_in = array();
            $page = $this->input->get('page'); //check required parameter
            $uuid = $this->input->get('uuid');
            if(!$uuid){
                errorResponse(required_data, REST_Controller::HTTP_BAD_REQUEST);
            }
            $page = ($page) ? $page : 1;
            $limit = 10;
            $offset = ($page == 1) ? 0 : $limit*($page-1);
            $checkUser = $this->common_model->GetColumnName('user', array('uuid' => $uuid, 'is_deleted'=> 0, 'status'=>1), array('id'));
            if (!$checkUser) {
                errorResponse("No data found ", REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
            $checkConnection = $this->db->query('select DISTINCT(id), user.name, (select COALESCE(CONCAT( "'.PROFILE_IMAGE.'", image), "") from images where user_id = user.id limit 1) as images from user WHERE FIND_IN_SET(id, (select COALESCE(GROUP_CONCAT(DISTINCT c1.contact_user_id), 0) as userId from common_connection c1 INNER JOIN common_connection c2 ON FIND_IN_SET(c1.phone,(select COALESCE(GROUP_CONCAT( DISTINCT phone ), 0)from common_connection where user_id = '.$user->id.')) where c1.user_id = '.$checkUser['id'].')) AND id != '.$checkUser['id'].' AND id != '.$user->id.' order by id DESC LIMIT '.$offset.','.$limit.'')->result_array();
            //echo $this->db->last_query(); exit;
            if ($checkConnection) {
                successResponse(" Common connection list ", REST_Controller::HTTP_OK, $checkConnection);
            } else {
                errorResponse(" No data found ", REST_Controller::HTTP_REQUEST_TIMEOUT);
            }
        }
    }

         /**
     * Function - Rearrange Image post
     * Description - Rearrange Image post
     * callingRoute - reArrangeImage
     * Created on - 27 jun 2022
     * @param {images} array of object { uuid and image index}
     * @param {*} res Response object used to send response
     * @returns Response data object along with json data
     */
    public function reArrangeImage_post() {
        if (!empty($this->user_data)) {
            $user = $this->user_data;
            $json = $this->Common_function->get_json_parameter(); //check required parameter
            //  print_r($json['images'], 'ddfd'); 
            //print_r($json->images); exit;
            if ($json) {
                for ($i=0; $i < count($json->images); $i++) { 
                    $this->common_model->UpdateData('images',array('uuid'=>$json->images[$i]->uuid, 'user_id'=> $user->id),array('image_index'=>$json->images[$i]->image_index));
                }
            }
            successResponse("Changs saved successfully!", REST_Controller::HTTP_OK);
        }
    }
}
