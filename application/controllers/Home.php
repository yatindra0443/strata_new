<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->check_login();
	}
	public function check_login(){
		if($this->session->userdata('admin_id')){
			redirect('Admin/dashboard');
		}
	}
	public function index(){
		$this->load->view('Admin/login');
	}

}
?>