<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Common_function extends CI_Model {

	public function __construct() {
		parent::__construct();
    $this->load->library('S3_upload');
    $this->load->library('S3');

	} 

  //Check all required parameter
  function get_json_parameter($not_required_value=false)
  {

      $json = file_get_contents('php://input');
      $json_array = json_decode($json);
  
      if(!empty($not_required_value))
      {
          $required_value = array_diff_assoc((array)$json_array,$not_required_value); //remove unrequired field
      }else
      {
          $required_value = $json_array;
      }
      
      if(count(array_filter((array)$required_value)) == count((array)$required_value)) //check required field not empty
      {
          return $json_array;
      }else
      {   //return 'params_error';
          $json_array = (object)array();
          errorResponse(required_data,400);
      }
  }

  //get form data parameter 
  function get_required_parameter($not_required_value=array(),$isAdmin='')  //get params 
  {
      $missing = array();
      unset($_POST['images[]']);
      foreach ($_POST as $key => $value) { 
        
          if ($value == "" && !array_key_exists($key,$not_required_value) ) { array_push($missing, $key); }
      }
      if (count($missing) > 0 || empty($_POST)) {
        if($isAdmin=='admin') {
        }else {
          errorResponse(required_data,400);   
        }
      }else
      {
          return $_POST;
      }
  }

  function get_user_image($image)
  {
      if(filter_var($image, FILTER_VALIDATE_URL))
      {
      }else
      {
          if(!empty($image))
          {
              $image = base_url().GET_USER_IMAGE_PATH.$image;
          }
      }
      return $image;
  }
  //get single image
  function get_image($image,$path)
  {
        if(filter_var($image, FILTER_VALIDATE_URL))
        {
            $imageurl = $image;
        }else {
            $imageurl = base_url().$path.$image;
        }  
      return $imageurl;
  }

  function get_multiple_images($table,$field,$where,$path)
  {
      $imageurl = array();
      $getImagesGallary = $this->db->select($field)->get_where($table,$where)->result();
      if(!empty($getImagesGallary))
      {
          foreach ($getImagesGallary as $key) {
              $imageurl[] = array(
                                  'id'=>$key->id,
                                  'images'=>base_url().$path.$key->image
                                  );
          }
      }
      return $imageurl;
  }

    function upload_images($path,$keyname='')
    {
        $image_url = $image_name = array();
        $file_count=count($_FILES[$keyname]['name']);
        $this->load->library('upload');
        for ($i=0; $i < $file_count; $i++)
        {  
            $_FILES['file_url']['name']= $_FILES[$keyname]['name'][$i];
            $_FILES['file_url']['type']= $_FILES[$keyname]['type'][$i];
            $_FILES['file_url']['tmp_name']= $_FILES[$keyname]['tmp_name'][$i];
            $_FILES['file_url']['error']= $_FILES[$keyname]['error'][$i];
            $_FILES['file_url']['size']= $_FILES[$keyname]['size'][$i];

            $config = array();
            $config['upload_path']   = $path;
            $config['allowed_types'] = 'jpg|jpeg|png';

            //$subFileName = explode('.',$_FILES['file_url']['name']);
            //$ExtFileName = end($subFileName);
            $images = str_replace(' ', '_', time().'_'.$_FILES['file_url']['name']);
            $config['file_name'] = $images;
            $image_name[] = $images;
            $image_url[] = base_url().$path.$images;
            move_uploaded_file($_FILES["file_url"]["tmp_name"],$path.$images);
            // $this->upload->initialize($config);
            // if (!$this->upload->do_upload('file_url')) {
            //     $errors = $this->upload->display_errors();
                
            //     //return $this->common_function->echoresponse('error','IMAGE_UPLOAD_ERROR',(object)array(),'errors');
            // }else
            // {
        
            // }
        }
        if(!empty($image_url))
        {
            return array('url'=>$image_url,'name'=>$image_name);
        }
    }

	
  function upload_image($path,$keyname='',$type='') //upload images
  {
      if($type!=''){
          $images = str_replace(' ', '', $_FILES[$keyname]['name']); //admin
      }else{
          $images = str_replace(' ', '',$_FILES[$keyname]['name']);
      }
      $dir = dirname($_FILES[$keyname]["tmp_name"]);
      $destination = $dir.'/'.$images;
      rename($_FILES[$keyname]["tmp_name"], $destination); 
      $upload = $this->s3_upload->upload_file($destination); 
      print_r($upload, 'dd'); exit;
      return $upload;
  }
	
  private function getGoogleAccessToken(){
    //$this->load->library('google');
    $credentialsFilePath = APPPATH . 'libraries/vendor/google-api-php-client/strata-efa17-6501bd47c55d.json'; //replace this with your actual path and file name
    $client = new \Google_Client();
    $client->setAuthConfig($credentialsFilePath);
    $client->addScope('https://www.googleapis.com/auth/firebase.messaging');
    $client->refreshTokenWithAssertion();
    $token = $client->getAccessToken();
    return $token['access_token'];
  }

  public function fcmPushNotification($title,$message,$user_id,$fcmToken){
    //$url = 'https://fcm.googleapis.com/fcm/send';
    $url = 'https://fcm.googleapis.com/v1/projects/strata-efa17/messages:send';
    $headers = [
      'Authorization: Bearer ' . $this->getGoogleAccessToken(),
      'Content-Type: application/json'
    ];

    $notification_tray = [
          'title'             => $title,
          'body'              => $message,
      ];

    $in_app_module = [
          "title"          => $title,
          "body"           => $message,
      ];
    //The $in_app_module array above can be empty - I use this to send variables in to my app when it is opened, so the user sees a popup module with the message additional to the generic task tray notification.
    $message = [
      'message' => [
          'token'            => $fcmToken,
          'notification'     => (object)$notification_tray,
          'data'             => (object)$in_app_module,
      ]
    ];
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode((object)$message));
    $result = curl_exec($ch);
    //echo $result;
    if ($result === FALSE) {
        //Failed
        //die('Curl failed: ' . curl_error($ch));
    }
    curl_close($ch);
  }

  // msg91 function
  //old flow send sms with DLT id
  public function send_msg($otp, $mobile_number){
    //Your authentication key
   $authKey = AUTH_KEY;
   $flowId = FLOW_ID;
   $mobileNumber = $mobile_number;
    // init the resource
   $curl = curl_init();

   curl_setopt_array($curl, [
   CURLOPT_URL => "https://api.msg91.com/api/v5/flow/",
   CURLOPT_RETURNTRANSFER => true,
   CURLOPT_ENCODING => "",
   CURLOPT_MAXREDIRS => 10,
   CURLOPT_TIMEOUT => 30,
   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
   CURLOPT_CUSTOMREQUEST => "POST",
   CURLOPT_POSTFIELDS => "{\n  \"flow_id\": \"$flowId\",\n  \"mobiles\": \"$mobileNumber\",\n  \"otp\": \"$otp\"\n}",
   CURLOPT_HTTPHEADER => [
       "authkey: $authKey",
       "content-type: application/json"
   ],
   ]);

   $response = curl_exec($curl);
   $err = curl_error($curl);

   curl_close($curl);

   if ($err) {
       echo "cURL Error #:" . $err;
   } else {
       //echo $response;
   }
 }
 
	
  /**
   * Simple PHP age Calculator
   * 
   * Calculate and returns age based on the date provided by the user.
   * @param   date of birth('Format:yyyy-mm-dd').
   * @return  age based on date of birth
   */
  function ageCalculator($dob){
      if(!empty($dob)){
          $birthdate = new DateTime($dob);
          $today   = new DateTime('today');
          $age = $birthdate->diff($today)->y;
          return $age;
      }else{
          return 0;
      }
  }

  function remove_utf8_bom($text)
  {
      $bom = pack('H*','EFBBBF');
      $text = preg_replace("/^$bom/", '', $text);
      return $text;
  }

}
