<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


//admin router
$route['Admin']='Admin/Login';
$route['Admin/login']='Admin/Login';
$route['admin/login']='Admin/Login';
$route['admin/login_user']='Admin/Login/do_login';
$route['admin/dashboard']='Admin/Home';
$route['admin/profile']='Admin/Profile';
$route['admin/logout']='Admin/Home/logout';
$route['admin/change_name_email'] = 'Admin/Profile/change_name_email';
$route['admin/change_password'] = 'Admin/Profile/change_password';
$route['admin/version'] = 'Admin/Version';

$route['admin/new-user'] = 'Admin/User';
$route['admin/active-user'] = 'Admin/User/active_user';
$route['admin/inactive-user'] = 'Admin/User/inactive_user';
$route['admin/approve-user'] = 'Admin/User/approve_user';
//$route['admin/reject-user'] = 'Admin/User/reject_user';
$route['admin/waiting-user'] = 'Admin/User/waiting_user';
$route['admin/change_Profile_status'] = 'Admin/User/change_Profile_status';
$route['admin/change_status'] = 'Admin/User/change_status';
$route['admin/add_waiting_list'] = 'Admin/User/add_waiting_list';
$route['admin/update_version'] = 'Admin/Version/update_app_version';
// api router

$route['login'] = 'api/Auth/login';
$route['verify_otp'] = 'api/Auth/verify_otp';
$route['send_otp'] = 'api/Auth/send_otp';
$route['signup'] = 'api/Auth/registration_step1';
$route['contact_list'] = 'api/Auth/contact_list';
$route['complete_profile'] = 'api/Auth/complete_profile';
$route['upload_images'] = 'api/Auth/upload_images';
$route['checkStatus'] = 'api/Auth/checkStatusCode';
$route['appVersion'] = 'api/Auth/appVersion';
$route['testNotification'] = 'api/Auth/testNotification';


$route['get_profile'] = 'api/User/profile';
$route['update_profile'] = 'api/User/update_profile';
$route['delete_account'] = 'api/User/delete_account';
$route['logout'] = 'api/User/logout';
$route['like_dislike'] = 'api/User/like_dislike';
$route['profile_setting'] = 'api/User/profile_setting';
$route['invite'] = 'api/Auth/invite';
$route['accept_reject_invite'] = 'api/User/accept_reject_invite';
$route['invite_list'] = 'api/User/invite_list';
$route['like_list'] = 'api/User/like_list';
$route['block_unblock'] = 'api/User/block_unblock';
$route['block_list'] = 'api/User/block_list';
$route['matching_list'] = 'api/User/matching_list';
$route['unmatch'] = 'api/User/unmatch';
$route['undo'] = 'api/User/undo';
$route['update_prefrence'] = 'api/User/update_prefrence';
$route['test'] = 'api/Auth/test';
$route['update_fcmToken'] = 'api/User/update_fcmToken';
$route['get_user_list_by_id'] = 'api/User/getUserListById';
$route['increase_screen_shot_count'] = 'api/User/increaseScreenShotCount';
$route['common_connection'] = 'api/User/commonConnection';
$route['reArrangeImage'] = 'api/User/reArrangeImage';