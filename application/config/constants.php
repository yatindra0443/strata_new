<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define("Project",'private-karmchari');
define("Email",'info@strata.org');
define('FCM_KEY','AAAADPEOhkU:APA91bHXyZ73tD06nogtTzzf4sNkQScsb-Ro9vbX98Vl-cQFAkpm_r66sPM7l3r2pEVAKPZQlGzDHsesqi-kdHBQC3Jv_ihy53YMkHgpjpJ6q8KmHP1B9xsP_3kDW8JgkWIzBrTTrvk3');
//Your authentication key
define('AUTHKEY',"YourAuthKey");

//GET IMage
define('PROFILE_IMAGE', "https://joinstrata.org/strata/assets/images/user_img/");
define('USER_IMAGE_FOLDER', "user_image");

define("login_success",'Login successfully');
define("otp_incorrect",'Otp is incorrect');
define("required_data",'Required data missing');
define("send_otp","Send otp in registred mobile number");
define("query_error","Something went wrong");
define("phone_notregistred","This number is not registred");
define("phone_required","Phone number is required");
define("phone_registred","This Phone number already exist");
define("email_exixts","This Email id already exist");
define("updated_data","Data updated successfully");
define("user_not_registred","This User not registerd");
define("add_image","Images added successfully");
define("image_validation","Please select minmum 4 and maximum 8 images");
define("success_registration", "registerd successfully");
define("profile_status_pending", "Your profile is in under review Please wait for approval");
define("profile_status_waitlisted", "Your profile has been waitlisted by administrater.");
define("Get_user", "User get successfully.");
define("update_profile", "Profile updated successfully");
define("account_delete", "Account deleted successfully!");
define("change_status", "Your account is deactive by administrater, for more infomation contact to administrater!");
define("contact_error", "Looks like you don't have any members in your contact book");
define("verify_otp", "Otp verify successfully!");
define("preference_updated", "Changes saved successfully");
define("image_extension_error", "Image type should be jpeg, jpg and png");
define('OTP_MSG', ' is your OTP to login to STRATA. Please do not share this with anyone. Velvet Rope Digital Communities LLP.');
define('SENDER_ID', 'VELROP');
define('AUTH_KEY', getenv('MSG_AUTH_KEY'));
define('DLT_ID', getenv('DLT_TEMP_ID'));
define('FLOW_ID', getenv('FLOW_ID'));