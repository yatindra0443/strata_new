<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function errorResponse($message, $statusCode,$data=null) {
  $result = array(
    "success"=> false,
    "status"=>$statusCode,
    "message"=>$message,
    "data"=>$data
  );
  header("content-type: application/json");
   echo json_encode($result); exit;
};


// Response handlers
function successResponse($message,$statusCode, $data=null) {
 $result =  array(
    "success"=> true,
    "status"=>$statusCode,
    "message"=>$message,
    "data"=>$data
  );
 header("content-type: application/json");
  echo json_encode($result); exit;
};


 function generateToken($length) {
   $result = "";
  $characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  $charactersLength = count($characters);
  for($i = 0; $i < $length; $i++) {
    $result += $characters.charAt(floor(random() * $charactersLength));
  }
  return $result;
};






