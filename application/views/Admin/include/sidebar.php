


<aside class="main-sidebar">
	<section class="sidebar" style="height: auto;">
		<!-- Sidebar user panel -->
		<div class="user-panel" style="height: 63px;">
			<div class="text-center info">
				<p><?php echo ucfirst($userdata['name']); ?></p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
			<!-- <li class="active">
				<a href="<?php echo site_url().'admin/home';?>">
					<i class="fa fa-dashboard"></i> <span>Dashboard</span>
				</a>
			</li> -->
			<li class="treeview">
				<a href="#">
				<i class="fa fa-drivers-license-o"></i><span>User management</span>
				<span class="pull-right-container">
					<i class="fa fa-medkit-left pull-right"></i>
				</span>
				</a>
				<ul class="treeview-menu">
				<li><a href="<?=base_url(); ?>admin/new-user"><i class="fa fa-circle-o"></i>New User</a></li>

				<li><a href="<?=base_url(); ?>admin/waiting-user"><i class="fa fa-circle-o"></i>Waiting User</a></li>

				<li><a href="<?=base_url(); ?>admin/approve-user"><i class="fa fa-circle-o"></i>Approve User</a></li>

				<!--  <li><a href="<?=base_url(); ?>admin/reject-user"><i class="fa fa-circle-o"></i>Reject User</a></li> -->
				
				<li><a href="<?=base_url(); ?>admin/active-user"><i class="fa fa-circle-o"></i>Active User</a></li>
			
				<li><a href="<?=base_url(); ?>admin/inactive-user"><i class="fa fa-circle-o"></i>Inactive User</a></li>
			
				</ul>
			</li>
			<li class="active">
				<a href="<?php echo site_url().'admin/version';?>">
					<i class="fa fa-mobile"></i> <span>App Version</span>
				</a>
			</li>
		</ul>
	</section>
</aside>