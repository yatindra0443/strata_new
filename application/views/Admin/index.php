<?php include_once('include/header.php'); ?> 
<div class="content-wrapper">
	<section class="content-header">
		<h1>Dashboard<small>Control panel</small></h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
   </section>
   
   <!-- Main content -->
   <section class="content">
		<?php
		echo $this->session->flashdata('msg');
		?>
		<!-- Small boxes (Stat box) -->
		<div class="row">
			
			<!-- <div class="col-lg-3 col-xs-6">
				
				<div class="small-box bg-blue">
					<div class="inner">
						<h3>0</h3>
						<p>Job</p>
					</div>
					<div class="icon">
						<i class="ion ion-bag"></i>
					</div>
					<a href="<?php echo site_url(); ?>factory-admin" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div> -->

			<!-- ./col -->
			<div class="col-lg-3 col-xs-6 hide" >
			<!-- small box -->
				<div class="small-box bg-yellow">
					<div class="inner">
						<h3>44</h3>
						<p>User Registrations</p>
					</div>
					<div class="icon">
						<i class="ion ion-person-add"></i>
					</div>
					<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
				</div>
			</div>
		 
			<!-- ./col -->
		</div>
			<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php include_once('include/footer.php'); ?>