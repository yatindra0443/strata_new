<?php 
include_once('include/header.php'); 
$user_id = $this->session->userdata('user_id');			
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>Profile<small>Preview</small></h1>
		<!-- <ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"><a href="#">Profile</a></li>
		</ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
		<?php echo $this->session->flashdata('msg'); ?>
		<div class="row">
			<!-- left column -->
			<div class="col-md-6">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Personal Information</h3>
					</div>
					<!-- /.box-header -->
					<!-- form start -->
				<form role="form" method="post" id="profile_form" action="<?php echo site_url().'admin/change_name_email'; ?>" enctype="multipart/form-data">
						<div class="box-body">
							 <div class="col-sm-12 col-md-12 col-lg-12">

                    <div class="form-group">

                     <input type="text" class="form-control" placeholder="Name *" value="<?php echo $userdata['name']; ?>" id="name" name="name" required="" >

                  </div>

                  </div>
                  

                  <div class="col-sm-12 col-md-12 col-lg-12">

                    <div class="form-group">

                     <input type="email" class="form-control" placeholder="Email *" readonly="" value="<?php echo $userdata['email']; ?>" id="contact-email" name="email" required>

                  </div>

                  </div><!-- /.col-lg-6 -->  

                  <div class="col-sm-12 col-md-12 col-lg-12">

                    <div  class="common-btn">

                        <button type="submit" class="hvr-sweep-to-right block_btn" >Submit <i class="fa fa-hand-o-right" aria-hidden="true"></i></button>

                     </div>

                  </div>
						</div>
						<div class="box-footer">
							<input type="submit" name="submit" id="submit" value="Update" class="btn btn-success">
						</div>
					</form>
				</div>
			</div>

			<div class="col-md-6">
				<!-- general form elements -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Change Password</h3>
					</div>
			
					<form role="form" method="post" action="<?php echo site_url().'admin/change_password'; ?>">
						<div class="box-body">
							<div id="error_pass"></div>
							<div class="form-group">
								<label class=" form-control-label">Current Password</label>
								<input type="password" name="admin_pass" id="Current_Password" class="form-control">
							</div>
							<div class="form-group">
								<label class=" form-control-label">New Password</label>
								<input type="password" name="New_Password" id="New_Password" class="form-control">
							</div>
							<div class="form-group">
								<label class=" form-control-label">Confirm Password</label>
								<input type="password" name="Confirm_Password" id="Confirm_Password" class="form-control">
							</div>
						</div>

						<div class="box-footer">
							<input type="submit" name="submit" id="submit" value="Change" class="btn btn-success">
						</div>
					</form>
				</div>
			</div>
		</div>
    </section>
</div>
 
<?php include_once('include/footer.php'); ?>
