<?php 
include_once('include/header.php'); 
?>
<style>
.tox-toolbar__primary, .tox-editor-header{
	display:none !important;
}
</style>
<div class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $pageName; ?><small>list</small></h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> <?php echo $pageName; ?> list</a></li>
			<li class="active"><?php echo $pageName; ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo $pageName; ?> List</h3>
					
				</div>
				<div class="err"><?php echo $this->session->flashdata('msg'); ?></div>
				<!-- /.box-header -->
				<div class="table-responsive">
					<table id="bootstrap-data-table" class="table table-striped table-bordered DataTable">
						<thead>
							<tr>
								<th>S.No.</th>
								<th> Name</th>
								<th>Email</th>
								<th>Phone</th>
								<th>Age</th>
								<th>gender</th>
								<th>Current city</th>
								<th>Orignal city</th>
								<th>Linkdin profile</th>
								<!-- <th>Worked link</th> -->
								<th>About us</th>
								<th>Membership code</th>
								<th>Instagram</th>
								<!-- <th>Intrsted in</th> -->
								<th>Created Date</th>
								<th>Profile Status</th>
								<th>Is approved</th>
								<?php if($this->uri->segment(2)!='reject-user'){ ?>
								<th>Change Profile Status</th>
								<th>Change Account Status</th>
							<?php } ?>
							</tr>
						</thead>
						<tbody>
							<?php
							if(count($user) > 0){
							foreach($user as $key => $value){
							
							?>
							<tr>
								<td><?php echo $key+1; ?></td>
								<td><?php echo $value['name']; ?></td>
								<td><?php echo $value['email']; ?></td>
								<td><?php echo $value['phone']; ?></td>
								<td><?php echo $value['age']; ?></td>
								<td><?php if($value['gender']=='1') { echo "Male";} else if($value['gender']=='2') { echo "Female";} else if($value['gender']=='3') { echo "Transgender";} else if($value['gender']==4) { echo "Non-Binary";} ?></td>
								<td><?php echo $value['current_city']; ?></td>
								<td><?php echo $value['orignal_city']; ?></td>
								<td><?php echo $value['linkdin_profile']; ?></td>
								<!-- <td><?php //echo $value['work_link']; ?></td> -->
								<td><?php echo $value['about_us']; ?></td>
								<td><?php echo $value['membership_code']; ?></td>
								<td><?php echo $value['instagram']; ?></td>
								<!-- <td><?php //if($value['interested_in']=='1') { echo "Men";} else if($value['interested_in']=='2') { echo "Women";} else if($value['interested_in']=='3') { echo "Transgender";} else if($value['interested_in']==4) { echo "Non-Binary";} ?></td> -->

								<td><?php echo date('d M Y',strtotime($value['created_at'])); ?></td>
								
								<td><?php if($value['status']=='1') { echo "Active";} else { echo "Inactive";} ?></td>

								<td><?php if($value['profile_status']=='2') { echo "Approved";} else if($value['profile_status']=='1') { echo "Waiting";} else { echo 'Pendding';} ?></td>

                               <?php if($this->uri->segment(2)!='reject-user'){ ?>
								<td>
									<?php if($this->uri->segment(2)=='new-user'){
									if($value['profile_status']==0){ ?>
										<a class="btn btn-info btn-xs" onclick="return change_Profile_status(<?php echo $value['id']; ?>,'2','Are you sure want to approve this user?')"  >Approve</a>

										<a class="btn btn-danger btn-xs" onclick="return change_Profile_status(<?php echo $value['id']; ?>,'1','Are you sure want to waiting this user?')" >Waiting</a>

										<!-- <a class="btn btn-warning btn-xs" onclick="return add_waiting_list(<?php //echo $value['id']; ?>,'Are you sure want to waiting this user?')" >Waiting</a> -->

									<?php } } ?>
                                      
                                      <?php if($this->uri->segment(2)=='waiting-user' && $value['profile_status']==1){ ?>

                                         <a class="btn btn-info btn-xs" onclick="return change_Profile_status(<?php echo $value['id']; ?>,'2','Are you sure want to approve this user?')"  >Approve</a>
										
										<?php } ?>
								</td>
								<td>
								<?php
									 if($value['status']==0){ ?>
										<a class="btn btn-success btn-xs" onclick="return change_status(<?php echo $value['id']; ?>,'1','Are you sure want to active this user?')" >Active</a>
									<?php } else { ?>
										<a class="btn btn-danger btn-xs" onclick="return change_status(<?php echo $value['id']; ?>,'0','Are you sure want to deactive ths user?')" >Deactive</a>
									<?php } ?>
								</td>
							<?php } ?>
							</tr>
							<?php } } ?>
							
							
						</tbody>
					</table>
					
				</div>
			</div>
			
		</div>
	</div>
	</section>
</div>

<?php include_once('include/footer.php'); ?>


<script type="text/javascript">
	function change_Profile_status(userId,status,msg){
		if (confirm(msg)){
			 $.ajax({
                url: "<?php echo site_url(); ?>admin/change_Profile_status",
                type: "POST",
                data: {
                  userId:userId,
                  status:status,
            },
            success: function (res) {
            	console.log(res);
                 if(res=='2'){
                 	window.location.href="<?php echo site_url(); ?>admin/approve-user";
                 } else if(res=='1'){
                 	window.location.href="<?php echo site_url(); ?>admin/waiting-user";
                 } else {
                 	window.location.href="<?php echo site_url(); ?>admin/new-user";
                 }
            }
        });
		}
      
	}

	function change_status(userId,status,msg){
		if (confirm(msg)){
			 $.ajax({
                url: "<?php echo site_url(); ?>admin/change_status",
                type: "POST",
                data: {
                  userId:userId,
                  status:status,
            },
            success: function (res) {
                 if(res==1){
                 	window.location.href="<?php echo site_url(); ?>admin/active-user";
                 } else if(res==2){
                 	window.location.href="<?php echo site_url(); ?>admin/inactive-user";
                 } else {
                 	window.location.href="<?php echo site_url(); ?>admin/active-user";
                 }
            }
        });
		}
      
	}

	function add_waiting_list(userId,msg){
		if (confirm(msg)){
			 $.ajax({
                url: "<?php echo site_url(); ?>admin/add_waiting_list",
                type: "POST",
                data: {
                  userId:userId,
            },
            success: function (res) {
                 if(res==1){
                 	window.location.href="<?php echo site_url(); ?>admin/waiting-user";
                 } else {
                 	window.location.href="<?php echo site_url(); ?>admin/new-user";
                 }
            }
        });
		}
      
	}
</script>