<?php 
include_once('include/header.php'); 
?>
<style>
.tox-toolbar__primary, .tox-editor-header{
	display:none !important;
}
</style>
<div class="content-wrapper">
	<section class="content-header">
		<h1><?php echo $pageName; ?><small>list</small></h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> <?php echo $pageName; ?> list</a></li>
			<li class="active"><?php echo $pageName; ?></li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title"><?php echo $pageName; ?> List</h3>
					
				</div>
				<div class="err"><?php echo $this->session->flashdata('msg'); ?></div>
				<!-- /.box-header -->
				<div class="table-responsive">
					<table id="bootstrap-data-table" class="table table-striped table-bordered DataTable">
						<thead>
							<tr>
								<th>S.No.</th>
								<th>Device</th>
								<th>Current Version</th>
								<th>Force Update</th>
								<th>Update</th>
							</tr>
						</thead>
						<tbody>
							<?php
							if(count($versionData) > 0){
							foreach($versionData as $key => $value){
							
							?>
							<tr>
								<td><?php echo $key+1; ?></td>
								<td><?php echo $value['device_type']; ?></td>
								<td>
									<div style="display:block" id="appVersion_<?php echo $value['id'];?>"><?php echo $value['currentVersion']; ?></div>
									<div style="display:none" id="appVersion_new_<?php echo $value['id'];?>"><input type="number" id="version_<?php echo $value['id'];?>" value="<?php echo $value['currentVersion'];?>"></div>
								</td>
								<td>
									<div id="forceUpdate_<?php echo $value['id'];?>" style="display:block">
										<?php if($value['forceUpdate']=='1') { echo "Yes"; }else { echo "No"; } ?>
									</div>
									<div id="forceUpdate_new_<?php echo $value['id'];?>" style="display:none">
										<select id="force_status_<?php echo $value['id'];?>" name="forceUpdate" class="form-control">
											<option id="yes" value="1" <?php if($value['forceUpdate']=='1') { echo 'selected'; }?>>Yes</option>
											<option id="no" value="0" <?php if($value['forceUpdate']=='0') { echo 'selected'; }?>>No</option>
										</select>
									</div>
								</td>
								<td>
								<a class="btn btn-info btn-xs" id="edit_<?php echo $value['id'];?>" style="display:block" onclick="return edit_version(<?php echo $value['id']; ?>)">Edit</a>
								<a class="btn btn-info btn-xs" id="save_<?php echo $value['id'];?>" style="display:none" onclick="return update_version(<?php echo $value['id']; ?>)">Save</a>
                  				</td>
							</tr>
							<?php } } ?>
							
							
						</tbody>
					</table>
					
				</div>
			</div>
			
		</div>
	</div>
	</section>
</div>

<?php include_once('include/footer.php'); ?>


<script type="text/javascript">
	function update_version(id){
		var appVersion = $("#version_"+id).val(); 
	    var forceUpdate = $("#force_status_"+id).val(); 
		if (confirm("Are you really want to update values")){
			 $.ajax({
                url: "<?php echo site_url(); ?>admin/update_version",
                type: "POST",
                data: {
                  id:id,
                  currentVersion:appVersion,
                  forceUpdate:forceUpdate,
            },
            success: function (res) {
            	console.log(res);
                 if(res=='true'){
                 	window.location.href="<?php echo site_url(); ?>admin/version";
                }
            }
        });
		}
		$("#edit_"+id).css("display","block"); 
	    $("#save_"+id).css("display","none");
		$("#appVersion_"+id).css("display","block"); 
	    $("#appVersion_new_"+id).css("display","none"); 
		$("#forceUpdate_new_"+id).css("display","none"); 
	    $("#forceUpdate_"+id).css("display","block");
      
	}
	function edit_version(id){
		$("#appVersion_"+id).css("display","none"); 
	    $("#appVersion_new_"+id).css("display","block"); 
		$("#edit_"+id).css("display","none"); 
	    $("#save_"+id).css("display","block"); 
		$("#forceUpdate_"+id).css("display","none"); 
	    $("#forceUpdate_new_"+id).css("display","block"); 
	}
</script>