<?php defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require_once APPPATH . '/libraries/REST_Controller.php';
require_once APPPATH . '/libraries/JWT.php';
require_once APPPATH . '/libraries/BeforeValidException.php';
require_once APPPATH . '/libraries/ExpiredException.php';
require_once APPPATH . '/libraries/SignatureInvalidException.php';


class BD_Controller extends REST_Controller
{
	private $user_credential;
    public function auth()
    {
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
    
        //JWT Auth middleware
        $headers = $this->input->get_request_header('Authorization');

        $kunci = $this->config->item('secret_key');
         //secret key for encode and decode
        $token= "token";
       	if (!empty($headers)) {
           $headers = str_replace("Bearer ", "",$headers);
           $check = $this->common_model->GetColumnName('user',array('token'=>$headers), array('token'));
          
        	if ($check) {
            $token = $check['token'];

        	}
    	} 
        try {

           $decoded = JWT::decode($token, $kunci, array('HS256'));
           $this->user_data = $decoded;
        } catch (Exception $e) {

            $invalid = ['status' => $e->getMessage()]; //Respon if credential invalid

            errorResponse($e->getMessage(), 401);//401
        }
    }
}