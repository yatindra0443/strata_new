-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 30, 2022 at 02:53 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `statra`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`) VALUES
(1, 'Superadmin', 'admin@gmail.com', '$2y$10$XekQR/50F48IORTcO1vS9eGAoZOxHLYz5MzWPHAeOOPC71LZt4JO2');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `user_id`, `image`) VALUES
(1, '0d3b6f1f-fab9-4d5d-9322-6261cbbc119d', '7b09f684e38b5f92d07cc883b1b0ba9c.jpeg'),
(2, '0d3b6f1f-fab9-4d5d-9322-6261cbbc119d', '064e31239c2ac0c06f17bbcb5cf48c0a.jpeg'),
(3, '0d3b6f1f-fab9-4d5d-9322-6261cbbc119d', '5397aa993db31036701e6194ea7df57a.jpeg'),
(4, '0d3b6f1f-fab9-4d5d-9322-6261cbbc119d', '72c8956f406cfdbcd361781876a8637f.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `uuid` varchar(100) NOT NULL,
  `step_completed` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `dob` varchar(100) NOT NULL,
  `gender` int(11) NOT NULL COMMENT '1=male,2=female,3=transgender,4=other',
  `age` int(11) NOT NULL,
  `current_city` varchar(100) NOT NULL,
  `orignal_city` varchar(100) NOT NULL,
  `linkdin_profile` varchar(100) NOT NULL,
  `work_link` varchar(100) NOT NULL,
  `about_us` text NOT NULL,
  `refreal_key` varchar(100) NOT NULL,
  `refreal_by` int(11) NOT NULL,
  `membership_code` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  `intrsted_in` int(11) NOT NULL,
  `otp_verify` int(11) NOT NULL COMMENT '1=verify',
  `status` int(11) NOT NULL DEFAULT 1 COMMENT '1=active,0=deactive',
  `user_status` int(11) NOT NULL COMMENT '1=approved,0=rejected',
  `otp` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uuid`, `step_completed`, `name`, `email`, `phone`, `password`, `dob`, `gender`, `age`, `current_city`, `orignal_city`, `linkdin_profile`, `work_link`, `about_us`, `refreal_key`, `refreal_by`, `membership_code`, `instagram`, `intrsted_in`, `otp_verify`, `status`, `user_status`, `otp`, `created_at`, `updated_at`) VALUES
(1, '0d3b6f1f-fab9-4d5d-9322-6261cbbc119d', 'step-1', 'Reshmi singh', 'reshmi.s@engineerbabu.in', '8770736678', '', '1995-03-23', 0, 0, 'indore', 'indore', '', 'http://localhost/instagram/', 'this is testing', '', 0, '', '', 0, 0, 1, 0, 3892, '2022-04-30 14:12:51', '2022-04-30 14:12:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
